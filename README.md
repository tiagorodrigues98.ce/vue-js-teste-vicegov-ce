# APP VUE => BUSCA FILMES

# Instalação NVM em ambiente linux ubuntu:
 - Instalação do nvm caso não possua:
 - curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash

 - Recarregue o bashrc:
 - source /home/$USER/.bashrc 

# Instalando uma versão node:
 - nvm install 14.16.1
 - npm install -g yarn

# Clonando o projeto:
 - git clone https://gitlab.com/tiagorodrigues98.ce/vue-js-teste-vicegov-ce.git busca-filmes
 - cd busca-filmes
 
# Instale as libs via yarn:
 - yarn install

# Iniciando o projeto via terminal:
 - yarn serve

 - Obs: O endereço do server local gerado pelo yarn, copie-o e cole no navegador para acessá-lo
 Exemplo: http://localhost:8080/ 